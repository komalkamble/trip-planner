class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  protect_from_forgery with: :exception
  before_filter :authenticate_user!

  def after_sign_in_path_for(user)
  	if user.email.equal? "komal.kamble099@gmail.com"
  		trains_path
  	else
  		# Update redirect after creation of user screen
  		logger.debug "------------"
  	    trains_path
  	end
  	
  end

end
