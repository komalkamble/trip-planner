json.array!(@cities) do |city|
  json.extract! city, :id, :arrival_station, :departure_station
  json.url city_url(city, format: :json)
end
