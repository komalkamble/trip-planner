json.array!(@trains) do |train|
  json.extract! train, :id, :name, :number
  json.url train_url(train, format: :json)
end
