class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :arrival_station
      t.string :departure_station
      t.references :train 
      t.timestamps null: false
    end
  end
end
