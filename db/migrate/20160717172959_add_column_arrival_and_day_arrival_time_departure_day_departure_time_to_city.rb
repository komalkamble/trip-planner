class AddColumnArrivalAndDayArrivalTimeDepartureDayDepartureTimeToCity < ActiveRecord::Migration
  def change
    add_column :cities, :arrival_day, :string, null: true, blank: true
    add_column :cities, :arrival_time, :time, null: true, blank: true
    add_column :cities, :departure_day, :string, null: true, blank: true
    add_column :cities, :departure_time, :time, null: true, blank: true
  end
end
