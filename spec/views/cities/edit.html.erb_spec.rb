require 'rails_helper'

RSpec.describe "cities/edit", type: :view do
  before(:each) do
    @city = assign(:city, City.create!(
      :arrival_station => "MyString",
      :departure_station => "MyString"
    ))
  end

  it "renders the edit city form" do
    render

    assert_select "form[action=?][method=?]", city_path(@city), "post" do

      assert_select "input#city_arrival_station[name=?]", "city[arrival_station]"

      assert_select "input#city_departure_station[name=?]", "city[departure_station]"
    end
  end
end
