require 'rails_helper'

RSpec.describe "cities/index", type: :view do
  before(:each) do
    assign(:cities, [
      City.create!(
        :arrival_station => "Arrival Station",
        :departure_station => "Departure Station"
      ),
      City.create!(
        :arrival_station => "Arrival Station",
        :departure_station => "Departure Station"
      )
    ])
  end

  it "renders a list of cities" do
    render
    assert_select "tr>td", :text => "Arrival Station".to_s, :count => 2
    assert_select "tr>td", :text => "Departure Station".to_s, :count => 2
  end
end
