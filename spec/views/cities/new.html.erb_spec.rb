require 'rails_helper'

RSpec.describe "cities/new", type: :view do
  before(:each) do
    assign(:city, City.new(
      :arrival_station => "MyString",
      :departure_station => "MyString"
    ))
  end

  it "renders new city form" do
    render

    assert_select "form[action=?][method=?]", cities_path, "post" do

      assert_select "input#city_arrival_station[name=?]", "city[arrival_station]"

      assert_select "input#city_departure_station[name=?]", "city[departure_station]"
    end
  end
end
