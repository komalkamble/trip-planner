require 'rails_helper'

RSpec.describe "cities/show", type: :view do
  before(:each) do
    @city = assign(:city, City.create!(
      :arrival_station => "Arrival Station",
      :departure_station => "Departure Station"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Arrival Station/)
    expect(rendered).to match(/Departure Station/)
  end
end
