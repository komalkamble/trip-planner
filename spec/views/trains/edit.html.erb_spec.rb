require 'rails_helper'

RSpec.describe "trains/edit", type: :view do
  before(:each) do
    @train = assign(:train, Train.create!(
      :name => "MyString",
      :number => 1
    ))
  end

  it "renders the edit train form" do
    render

    assert_select "form[action=?][method=?]", train_path(@train), "post" do

      assert_select "input#train_name[name=?]", "train[name]"

      assert_select "input#train_number[name=?]", "train[number]"
    end
  end
end
