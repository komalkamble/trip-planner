require 'rails_helper'

RSpec.describe "trains/index", type: :view do
  before(:each) do
    assign(:trains, [
      Train.create!(
        :name => "Name",
        :number => 1
      ),
      Train.create!(
        :name => "Name",
        :number => 1
      )
    ])
  end

  it "renders a list of trains" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
