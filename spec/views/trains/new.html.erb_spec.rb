require 'rails_helper'

RSpec.describe "trains/new", type: :view do
  before(:each) do
    assign(:train, Train.new(
      :name => "MyString",
      :number => 1
    ))
  end

  it "renders new train form" do
    render

    assert_select "form[action=?][method=?]", trains_path, "post" do

      assert_select "input#train_name[name=?]", "train[name]"

      assert_select "input#train_number[name=?]", "train[number]"
    end
  end
end
