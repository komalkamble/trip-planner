require 'rails_helper'

RSpec.describe "trains/show", type: :view do
  before(:each) do
    @train = assign(:train, Train.create!(
      :name => "Name",
      :number => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
  end
end
